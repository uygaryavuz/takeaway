package takeaway;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestHelper {
	public WebDriver driver = null;
	public WebDriverWait wait = null;
	
	// Constructor
	public TestHelper(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
	}
	
	//Methods

	//click method
	public void click(By elementlocation) {
		wait.until(ExpectedConditions.elementToBeClickable(elementlocation)).click();
	}
	
	//write text method
	public void writeText(By elementlocation, String texttowrite) {
		wait.until(ExpectedConditions.elementToBeClickable(elementlocation)).click();
		wait.until(ExpectedConditions.elementToBeClickable(elementlocation)).clear();
		wait.until(ExpectedConditions.elementToBeClickable(elementlocation)).sendKeys(texttowrite);
	}
	
	//read text method
	public String readText(By elementlocation) {
		return wait.until(ExpectedConditions.elementToBeClickable(elementlocation)).getText();
	}
	
	//check if an element exist method
	public boolean checkIfExist(By elementlocation) {
		if(driver.findElements(elementlocation).size() != 0) {
			return true;
		}
		else 
			return false;
	}
	
	//get a random email
	public String getrandomEmail() {
		Random randomGenerator = new Random();
		int randomInt = randomGenerator.nextInt(100000);
		String email = "username" + randomInt + "@gmail.com";
		return email;
	}
	
	//get value of an attribute of web element
	public String getAttributeValue(By elementlocation, String attribute) {
		return driver.findElement(elementlocation).getAttribute(attribute);
	}
	
	public void pause(Integer milliseconds) {
		try {
			TimeUnit.MILLISECONDS.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
