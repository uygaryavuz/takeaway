package takeaway;


import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;



public class Tests extends TestBase {


	@Test(groups = {"First2Run"},enabled = true, retryAnalyzer = Retry.class, description = "A long Happy Path")
	public void happyPath() {
		test = extent.createTest("A long Happy Path");
		test.log(Status.INFO, "in homepage");
		homePage.changeLang();
		test.log(Status.INFO, "in homepage and changed lang to ENG");
		homePage.enterAddress("8889AA");
		test.log(Status.INFO, "in restaurant list page for entered address");
		restaurantListPage.selectRest("QA Restaurant Selenium");
		test.log(Status.INFO, "in restaurant main page and showing menu");
		restaurantMainPage.selectProducts("Coke");
		test.log(Status.INFO, "Coke added");
		restaurantMainPage.selectProducts("Duck Breast");
		test.log(Status.INFO, "Duck Breast with Tomato added");
		restaurantMainPage.selectProducts("Salami");
		test.log(Status.INFO, "Salami Pizza added");
		restaurantMainPage.gotoBasket();
		test.log(Status.INFO, "showing the basket details");
		restaurantMainPage.addNoteToProduct("No sugar, please");
		test.log(Status.INFO, "product note added");
		restaurantMainPage.goToCheckout();
		test.log(Status.INFO, "in checkout page now");
		checkoutPage.addressToCheckout();
		test.log(Status.INFO, "address infos added");
		checkoutPage.personalInfo();
		test.log(Status.INFO, "personal info added");
		checkoutPage.chooseDeliveryTime();
		test.log(Status.INFO, "delivery time choosen");
		checkoutPage.addRemark("Please, hurryy. I'm starving");
		test.log(Status.INFO, "a remark added");
		checkoutPage.selectPaymentMethod("Ideal");
		test.log(Status.INFO, "payment method selected as IDEAL");
		checkoutPage.completeOrder();
		test.log(Status.INFO, "forwarding to banks payment page");
		checkoutPage.paymentviaIdeal();
		test.log(Status.INFO, "cancelled the IDEAL method and returned to checkout");
		checkoutPage.addRemark("Please, hurryy. I'm starving");
		test.log(Status.INFO, "a remark added again, because it is disappearing");
		checkoutPage.selectPaymentMethod("cash");
		test.log(Status.INFO, "cash payment selected in checkout page");
		checkoutPage.completeOrder();
		orderSuccessPage.verifyOrderSuccess();
		test.log(Status.INFO, "order completed successfully");
		
			
	}
}
