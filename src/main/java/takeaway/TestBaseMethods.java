package takeaway;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import pages.CheckoutPage;
import pages.HomePage;
import pages.OrderSuccessPage;
import pages.RestaurantListPage;
import pages.RestaurantMainPage;

public class TestBaseMethods {
	public WebDriver driver;
	public WebDriverWait wait;
	public JavascriptExecutor js;
	
	public String baseurl = "https://www.Thuisbezorgd.nl";
	
	public String workingDir = System.getProperty("user.dir");
	public DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
	public Date date = new Date();
	public String ssPath = workingDir + "\\ScreenShots\\SS_" + dateFormat.format(date);
	public String testreportpath = workingDir + "\\ResultReports\\Report_" + dateFormat.format(date);

	
	HomePage homePage;
	RestaurantListPage restaurantListPage;
	RestaurantMainPage restaurantMainPage;
	CheckoutPage checkoutPage;
	OrderSuccessPage orderSuccessPage;
	
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	ExtentTest test;
	
	
	public void pagesObject() {
		homePage = new HomePage(driver, wait);
		restaurantListPage = new RestaurantListPage(driver, wait);
		restaurantMainPage = new RestaurantMainPage(driver, wait);
		checkoutPage = new CheckoutPage(driver, wait);
		orderSuccessPage = new OrderSuccessPage(driver, wait);
	}
	
	// Go to URL
	public void gotoBaseURL() {
		driver.get(baseurl);
	}
	
	//get system property simplified method
	public String getSysPropty(String key) {
		return System.getProperty(key);
	}
	
	// create sub folder for screenshots
	public void createFolderforScreenshots() {
		System.setProperty("ssPath", ssPath + "\\");
		new File(getSysPropty("ssPath")).mkdirs(); // create a folder under screenshots folder as named via current date
	}

	// take screenshot and save as specialized named png
	public void takeScreenshotandSaveAs(String specialize) {
		File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE); // get screenshot
		try {
			FileUtils.copyFile(src, new File(getSysPropty("ssPath") + specialize)); // copy screenshot to directory and
																					// rename it
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// create sub folder for test reports
	public void createFolderforTestReports() {
		System.setProperty("testreportpath", testreportpath + "\\");
		new File(testreportpath).mkdirs(); // create a folder under result reports folder as named via current date
	}
	
	// extent report initializer --create a folder and put the report in it
	public void extentReport(String browsername) {
		createFolderforTestReports();
		htmlReporter = new ExtentHtmlReporter(testreportpath + "/AutomationReport"+browsername+".html");
		htmlReporter.loadXMLConfig("./extent-config.xml");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
	}
	
	//extent report generator due to steps' and cases' result
	public void reportResultGenerator(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			test.log(Status.FAIL, result.getName() + " failed");
			test.fail(result.getThrowable());
			takeScreenshotandSaveAs(result.getName() + " - " + result.getMethod().getDescription() + " fail page.png");
		}
		else if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, result.getName() + " - " + result.getMethod().getDescription() + " case is passed");
		}
		else {
			test.log(Status.SKIP, result.getName() + " - " + result.getMethod().getDescription() + " case is skipped");		
		}
	}
	
	public void clearCookies() {
		gotoBaseURL();  //the reason we use gotobaseurl method here is when case failed in bank related pages and try to clear cache, logically clears bank payment related page's
		js.executeScript("localStorage.clear();");
		driver.manage().deleteAllCookies();  //clearing any related cookies	
	}
}
