package takeaway;
import java.io.IOException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

public class TestBase extends TestBaseMethods {
	
	//Runs at first of execution
	@BeforeTest
	@Parameters(value = {"browser"})
	public void beforeTest(String browser) throws Exception {
		if (browser == null) {
			browser = "chrome";
		}
		if (browser.startsWith("chrome") || browser.contains("chrome")) {
			browser = "chrome";
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			driver = new ChromeDriver();
		}
		if (browser.startsWith("firefox") || browser.contains("firefox")) {

			System.setProperty("webdriver.firefox.driver", "geckodriver.exe");
			FirefoxOptions options = new FirefoxOptions().setProfile(new FirefoxProfile());
			driver = new FirefoxDriver(options);
		}
		
		driver.manage().window().maximize();
		wait = new WebDriverWait(driver, 20);
		js = (JavascriptExecutor) driver;
		pagesObject();
		createFolderforScreenshots();
		extentReport(browser);
	}
	
	//Runs before each test case's occurance
	@BeforeMethod
	public void beforeMethod() {
		gotoBaseURL();
	}
	
	//Runs After each test case's occurance
	@AfterMethod
	public void afterMethod(ITestResult result) throws IOException {
		reportResultGenerator(result);
		clearCookies();
	}
	
	//Runs after finish of whole tests
	@AfterTest
	public void afterTest() {
		extent.flush();
		driver.quit();
	 }
}
