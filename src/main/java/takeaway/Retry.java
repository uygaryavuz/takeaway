package takeaway;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class Retry implements IRetryAnalyzer {	
	int count = 0;
	int maxtry = 3;
	
	public boolean retry(ITestResult iTestResult) {
		if(!iTestResult.isSuccess()) {
			if(count < maxtry) {
				count++;
				iTestResult.setStatus(ITestResult.FAILURE);
				return true;
			}
			else
				iTestResult.setStatus(ITestResult.FAILURE);
		}
		else
			iTestResult.setStatus(ITestResult.SUCCESS);
		return false;
	}
}

