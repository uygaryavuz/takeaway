package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import takeaway.TestHelper;

public class HomePage extends TestHelper {
	// Constructor
	public HomePage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}

	// Page Variables
	

	// WebElements' Locations
	public By changelanguagebtn = (By.id("locale"));  //change language point at the right top of page
	public By englishlangchoice = (By.cssSelector("[href='\\/en\\/']"));  //English language href 
	public By privacycookiepopupclosebtn = (By.id("privacybannerhref")); //close btn for privacy pop-up cookie warning coming at start
	public By addressarea = (By.id("imysearchstring"));  //area search bar to enter address text like Amsterdam etc.
	public By firstelementofaddresssuggestiondropdown = (By.xpath("//div[@id='iautoCompleteDropDownContent']//span[1]"));  //first element of address suggestion due to entered text
		
	//Page Methods
	
	//change language for web page
	public void changeLang() {
		click(changelanguagebtn);
		click(englishlangchoice);
		closePrivacyWarning();
	}
	
	//close privacy settings cookie warning
	public void closePrivacyWarning() {
		if(checkIfExist(privacycookiepopupclosebtn)) {
			click(privacycookiepopupclosebtn);
		}
	}
	
	//enter an area to view rest list
	public void enterAddress(String address) {
		writeText(addressarea, address);
		click(addressarea);
		click(firstelementofaddresssuggestiondropdown);
	}
}
