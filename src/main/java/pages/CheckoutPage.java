package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import takeaway.TestHelper;

public class CheckoutPage extends TestHelper {
	//Constructor
	public CheckoutPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}
	
	// Page Variables
	

	// WebElements' Locations
	public By addressarea = (By.id("iaddress")); // address area
	public By townarea = (By.id("itown"));  //town area
	public By postcodearea = (By.id("ipostcode")); //post code area
	public By namesurnamearea = (By.id("isurname")); //name surname enter area
	public By emailarea = (By.id("iemail"));  //email enter area
	public By phonenumberarea = (By.id("iphonenumber"));  //phone number enter area
	public By remarkarea = (By.id("iremarks")); //remark area
	public By cashpaymentchoice = (By.cssSelector(".paymentmethod0 label:nth-child(1)"));  //cash payment element
	public By idealpaymentchoice = (By.cssSelector(".paymentmethod3 label:nth-child(1)"));  //IDEAL payment method element
	
	public By idealpagecookiewarningaccept = (By.cssSelector(".mlf-js-cookie-accept")); //ideal payment page welcoming cookie install warning acccept
	public By cancelidealpaymentmethodbutton = (By.cssSelector(".ocf-btn-cancel .ng-scope")); //cancel choice for ideal payment
	public By confirmcancellationforidealpayment = (By.cssSelector("[data-ng-click='handleButtonAction\\(\\'YES\\'\\)']")); //"Ja"/Yes choice for cancallation of ideal payment method
	
	public By returntocheckoutchoice = (By.xpath("//p[@id='iresponsediv']/a[@href='/en/checkout-order-qa-restaurant-selenium']"));  //return to checkout href after cancellation of any other payment method than cash
	
	public By completeandgiveorderbtn = (By.cssSelector(".button_form.cartbutton-button")); //finalize and give order button
	
	
	//Page Methods
	
	//write address infos
	public void addressToCheckout() {
		writeText(addressarea, "my home address 432");
		writeText(townarea, "hometown");
		if(!(readText(postcodearea).contains("8889AA"))) {
			writeText(postcodearea, "8889AA");
		}
	}
	
	//how can we reach you area
	public void personalInfo() {
		writeText(namesurnamearea, "asdasd sadasdas");
		writeText(emailarea, getrandomEmail());
		writeText(phonenumberarea,"1111111111111");
	}
	
	//when would you like your fodd
	public void chooseDeliveryTime() {
		click(By.id("ideliverytime"));
		click(By.cssSelector("#ideliverytime > option:nth-child(3)"));
	}
	
	public void addRemark(String remarktosend) {
		writeText(remarkarea, remarktosend);
	}
	public void selectPaymentMethod(String paymentmethod) {
		if(paymentmethod.contains("cash")) {
			click(cashpaymentchoice);  //select cash payment
		}
		if(paymentmethod.contains("Ideal")) {
			click(idealpaymentchoice);  //select IDEAL payment method
		}
	}
	//cancel the IDEAL payment method choice and return to checkout
	public void paymentviaIdeal() {
		if(checkIfExist(idealpagecookiewarningaccept)) {
			click(idealpagecookiewarningaccept);  //accept cookie settings
		}
		click(cancelidealpaymentmethodbutton); //click cancel
		//if(checkIfExist(confirmcancellationforidealpayment)) {  //approve cancellation via IDEAL
			click(confirmcancellationforidealpayment); 
		//}
		click(returntocheckoutchoice);
	}
	
	//complete order or go to via card payment pages
	public void completeOrder() {
		click(completeandgiveorderbtn);
		pause(1000);
		
	}
	
	
}
