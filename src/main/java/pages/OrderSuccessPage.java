package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import takeaway.TestHelper;

public class OrderSuccessPage extends TestHelper {
	//Constructor
	public OrderSuccessPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}
	
	//Page Locators
	public By ordersuccestext = (By.cssSelector("#scoober-tracker > div > div > div > div.css-1q1xq2l > div.css-11882c3 > div.css-uy3k1q > h1"));  //order success text after checkout
	
	//Page Variables
	
	//Page Methods
	public void verifyOrderSuccess() {
		SoftAssert softAssert = new SoftAssert();
		softAssert.assertEquals(readText(ordersuccestext), "dsa dfsdf fds");
		
		Assert.assertTrue(readText(ordersuccestext).contains("Thank you"));
	}

}
