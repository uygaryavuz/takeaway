package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import takeaway.TestHelper;

public class RestaurantMainPage extends TestHelper {
	//Constructor
	public RestaurantMainPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}
	
	// Page Variables
	public JavascriptExecutor js =(JavascriptExecutor) driver;
	
	

	// WebElements' Locations
	public By tomatocheckboxasaddition = (By.cssSelector(".sidedish-checkbox:nth-child(2) .checkbox-inline > span:nth-child(2)"));  //tomato's locator as an addition for "Duck Breast"
	public By addbtnfordetailedproducttobasket = (By.cssSelector(".button_add_value h3")); //locator for a detailed(has additionals) product addition
	public By edit_addnotetothirdproduct = (By.xpath("//div[@id='products']/div[3]/div[1]//div[@class='cart-meal-edit-comment']")); //add note to third product 
	public By productcommentarea = (By.xpath("//div[@id='products']/div[3]/div[@class='cart-row commentinput']//textarea[@class='cart-meal-textarea']")); // third product's comment area
	public By addbtntoaddenteredtextascomment = (By.xpath("//div[@id='products']/div[3]/div[@class='cart-row commentinput']//a[.='Add']")); //add button to save entered comment for the product
	public By showbasketdetailsbtn = (By.id("btn-basket")); //show basket details btn at navigation bar
	public By gotocheckoutbutton = (By.xpath("//div[@id='ibasket']//section[@class='cartbutton']/a[@href='#']")); //finalize basket and go to checkout button
	
	
	//Page Methods
	
	
	//Returns id of searched product
	public String getProductId(String producttofoundid) {
		String jstemplate = "var id=\"\";document.querySelectorAll(\".meal\").forEach(e=>{\""+producttofoundid+"\"==e.querySelector(\".meal-name\").innerText.trim()&&(id=e.id)});";
		String idtosearch = (String) js.executeScript(jstemplate+"return id;").toString();
		return idtosearch;

	}
	
	/*//An static method for list of products to add basket
	public void selectProducts(String producttosearch) { 			
		
		click(By.cssSelector("[anchor-id='category_Drinks'] [itemtype='http\\:\\/\\/schema\\.org\\/Product']:nth-of-type(1) [itemprop='name']")); //addd coke
		click(By.cssSelector("[anchor-id='category_Meal_deals'] .menu-meal-additions")); // select duck breast
		click(By.cssSelector(".sidedish-checkbox:nth-child(2) .checkbox-inline > span:nth-child(2)")); //select tomato checkbox as addon
		click(addbtnfordetailedproducttobasket); //add detailed product to basket
		click(searchbar); //click to search bar
		writeText(searchbarareatowrite, "Salami"); //write text to search area
		click(By.cssSelector("[anchor-id='category_Pizzas'] [itemtype='http\\:\\/\\/schema\\.org\\/Product']:nth-of-type(1) .meal-description-texts"));  //select Salami pizza

	}
*/
	//More dynamic way of adding product to basket  *we need to write a more dynamic way of finding dropdown items and checkbox as additionals
	public void selectProducts(String producttosearch) {			
		click(By.id(getProductId(producttosearch)));
		if(producttosearch.equals("Duck Breast")) {
			pause(1000);
			//js.executeScript("document.querySelector(\"#Q1N30PNRN [data-name=Tomato]\").click();"); //can convert to more dynamic structure as we know the id(getproductid() function and needed addon like Potato, Tomato
			click(tomatocheckboxasaddition); //select tomato checkbox as addon
			click(addbtnfordetailedproducttobasket); //add detailed product to basket
		}
	}
	
	//show basket details
	public void gotoBasket() {
		click(showbasketdetailsbtn);
	}
	
	//add note to third product
	public void addNoteToProduct(String productnote) {
		click(edit_addnotetothirdproduct);  //add note to third product via edit button
		writeText(productcommentarea, productnote);
		click(addbtntoaddenteredtextascomment);
	}
	
	public void goToCheckout() { //click to order button at basket
		click(gotocheckoutbutton);
	}
	
}
