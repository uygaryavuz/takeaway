package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import takeaway.TestHelper;

public class RestaurantListPage extends TestHelper {
	//Constructor
	public RestaurantListPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}
	
	// Page Variables
	

	// WebElements' Locations
	public By restnametextinrestmainpage = (By.className("title-delivery"));  //Restaurant Main Page restaurant title/name

			
	//Page Methods
	
	//Select specified rest
	public void selectRest(String restnametosearch) {
		click(By.linkText(restnametosearch));
		Assert.assertEquals(readText(restnametextinrestmainpage), restnametosearch);  //Assert that we are in correct rest detail page
	}
}
